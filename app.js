"use strict"

const
  Router = require('koa-router'),

  koa = require('koa'),
  app = koa();

// Logs to the console "2014-11-18T01:53:48.058Z - GET - /"
function *reqlogger(next){
  console.log('%s - %s - %s', new Date().toISOString(), this.req.method, this.req.url);
  yield next;
}

app.use(reqlogger);

app.use(Router(app)); // Make sure that the middleware you want all routes to use is on top of the rest
app.get('/', function *(){
  console.log('Express-style example');
  this.body = "This is root page ('/')";
})

app.get('/:category/:id', function *(){
  console.log();
  var prms = this.params
  this.body = prms.category + prms.id;
})



const publicRouter = new Router(); // can be called securedRouter e.g. for all routes that need to be authenticated before access
publicRouter.get('/auth/github', function *(){
  console.log('Middleware-style Example');
  this.body = 'Authenticate with Github OAUTH API (Coming Soon)';
});

app.use(publicRouter.middleware());

app.use(function *(){
  this.body = 'Hello World';
});

app.listen(3000);